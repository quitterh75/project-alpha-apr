from django.urls import path
from projects.views import project_view, show_project, create_project

urlpatterns = [
    path("projects/", project_view, name="list_projects"),
    path("projects/<int:id>/", show_project, name="show_project"),
    path("projects/create/", create_project, name="create_project"),
]
